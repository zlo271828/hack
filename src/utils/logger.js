import bunyan from 'bunyan';
import PrettyStream from 'bunyan-prettystream';

const prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout);

const log = bunyan.createLogger({
    name: 'HACK',
    streams: [{
        level: 'debug',
        type: 'raw',
        stream: prettyStdOut,
    }],
});

export default log;
/*
    log.info
    log.warn
    log.error
*/
