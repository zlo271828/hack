import {Professor} from '../../libs/sequelize';

exports.delete = async function(ctx) {
    try {
        professor = await Professor.findOne({
            where: {
                id: ctx.params.id,
            },
        });
    } catch (err) {
        ctx.body = 'not found';
        return;
    }
    try {
        await professor.destroy();
        ctx.status = 200;
    } catch (e) {
        ctx.status = 404;
    }
};
