import {Professor} from '../../libs/sequelize';

exports.patch = async function(ctx) {
    let professor;
    try {
        professor = await Professor.findOne({
            where: {
                id: ctx.params.id,
            },
        });
    } catch (err) {
        ctx.body = 'not found';
        return;
    }
    try {
        professor.update(Object.assign({}, ctx.request.body));
        ctx.status = 200;
    } catch (e) {
        ctx.status = 404;
    }
};
