import {Student} from '../../libs/sequelize';

exports.patch = async function(ctx) {
    let student;
    try {
        student = await Student.findOne({
            where: {
                id: ctx.params.id,
            },
        });
    } catch (err) {
        ctx.body = 'not found';
        return;
    }
    try {
        student.update(Object.assign({}, ctx.request.body));
        ctx.status = 200;
    } catch (e) {
        ctx.status = 404;
    }
};
