import {Student} from '../../libs/sequelize';

exports.get = async function(ctx) {
    let student;
    try {
        student = await Student.findById(ctx.params.id, {
            attributes: [
                'id',
                'firstName',
                'lastName',
                'email',
                'avatarURL',
            ],
        });
    } catch (err) {
        ctx.body = 'not found';
    }
    ctx.body = student;
};
