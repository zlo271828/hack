import Students from '../../models/Student';

exports.get = async function(ctx) {
    let students;
    try {
        students = await Students.findAll({
            where: {
                group: ctx.params.group.toUpperCase(),
            },
            attributes: [
                'id',
                'firstName',
                'lastName',
                'email',
                'avatarURL',
            ],
        });
    } catch (err) {
        ctx.body = 'not found';
    }
    ctx.body = students;
};
