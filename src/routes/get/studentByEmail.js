import {Student} from '../../libs/sequelize';

exports.get = async function(ctx) {
    let student;
    try {
        student = await Student.findOne({
            where: {
                email: ctx.params.email,
            },
            attributes: [
                'id',
                'firstName',
                'lastName',
                'email',
                'avatarURL',
            ],
        });
    } catch (err) {
        ctx.body = 'not found';
    }
    ctx.body = student;
};
