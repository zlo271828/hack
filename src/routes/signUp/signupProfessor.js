import {Professor} from '../../libs/sequelize';
import logger from '../../utils/logger';

exports.post = async function(ctx) {
    try {
        await Professor.create({
            firstName: ctx.request.body.firstName,
            lastName: ctx.request.body.lastName,
            email: ctx.request.body.email,
            passwordHash: ctx.request.body.password,
            about: ctx.request.body.about,
            avatarURL: ctx.request.body.avatarURL,
        });
        logger.info('User: ' + ctx.request.body.email + ' successfully added!');
        ctx.status = 201;
    } catch (e) {
        logger.error(e);
        ctx.status = 400;
        ctx.body = 'This user already exist, or data is not valid.';
    }
};
