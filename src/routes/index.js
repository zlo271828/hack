import Router from 'koa-router';
import signinProfessor from './signIn/signinProfessor';
import signinStudent from './signIn/signinStudent';
import signupProfessor from './signUp/signupProfessor';
import signupStudent from './signUp/signupStudent';
import {graphiqlKoa, graphqlKoa} from 'apollo-server-koa';
import Schema from '../libs/graphQL/';

const router = new Router();

// graphQL
// router.post('/graphql', graphqlKoa({schema: Schema}));
router.post('/graphql', graphqlKoa((req) => {
    return {
        schema: Schema,
        context: req.req,
    };
},
));
router.get('/graphql', graphqlKoa({schema: Schema}));
router.get(
    '/graphiql',
    graphiqlKoa({
        endpointURL: '/graphql', // a POST endpoint that GraphiQL will make the actual requests to
    }),
);

// signIn
router.post('/signin/professor', signinProfessor.post);
router.post('/signin/student', signinStudent.post);

// signUp
router.post('/signup/professor', signupProfessor.post);
router.post('/signup/student', signupStudent.post);

module.exports = router;
