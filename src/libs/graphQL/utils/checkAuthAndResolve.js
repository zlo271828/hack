import config from 'config';
import jwt from 'jsonwebtoken';
import {AuthorizationError} from './../errors';
import db from '../../sequelize';

const checkAuthAndResolve = async (context, controller) => {
    const token = context.headers.authorization;
    if (!token) {
        throw new AuthorizationError({
            message: `You must supply a JWT for authorization!`,
        });
    }
    const decoded = jwt.verify(
        // token.replace('Bearer ', ''),
        token,
        config.jwtSecret,
    );
    console.log(decoded);
    let user = await db.Professor.findOne({where: {email: decoded.user.email}});
    if (!user) user = await db.Student.findOne({where: {email: decoded.user.email}});
    if (!user) {
        throw new AuthorizationError({
            message: `You must supply a valid JWT for authorization!`,
        });
    }
    console.log(user);
    return controller.apply(this, [decoded]);
};

export default checkAuthAndResolve;
