import checkAuthAndResolve from './checkAuthAndResolve';
import checkScopesAndResolve from './checkScopesAndResolve';

export default {checkScopesAndResolve, checkAuthAndResolve};
