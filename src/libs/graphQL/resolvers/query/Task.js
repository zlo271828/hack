import controllers from '../../controllers/index';
import checkScopesAndResolve from '../../utils/checkScopesAndResolve';

const {getAllTasksByStudent} = controllers;

export default {
    student(args, _, context) {
        return checkScopesAndResolve(context, ['read:student'], getAllTasksByStudent, args);
    },
};
