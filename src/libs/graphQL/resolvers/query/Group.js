import controllers from '../../controllers/index';
import checkScopesAndResolve from '../../utils/checkScopesAndResolve';

const {getSubjectByGroup, getAllTeamsByGroup, getPreapostorByGroup} = controllers;

export default {
    subject: (args, _, context) => {
        return checkScopesAndResolve(context, ['read:professor', 'read:student'], getSubjectByGroup, args);
    },
    teams(args, _, context) {
        return checkScopesAndResolve(context, ['read:professor', 'read:student'], getAllTeamsByGroup, args);
    },
    praepostor(args, _, context) {
        return checkScopesAndResolve(context, ['read:professor', 'read:student'], getPreapostorByGroup, args);
    },
};
