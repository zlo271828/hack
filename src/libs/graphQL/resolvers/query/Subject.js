import controllers from '../../controllers/index';
import checkScopesAndResolve from '../../utils/checkScopesAndResolve';

const {getProfessorBySubject, getAllGroupsBySubject} = controllers;

export default {
    professor(args, _, context) {
        return checkScopesAndResolve(context, ['read:student'], getProfessorBySubject, args);
    },
    groups(args, _, context) {
        return checkScopesAndResolve(context, ['read:professor'], getAllGroupsBySubject, args);
    },
};
