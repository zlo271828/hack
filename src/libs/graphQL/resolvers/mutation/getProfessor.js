export default {
    addSubject: async (root, {input}, context) => {
        return await root.createSubject(input);
    },
    getSubject: async (root, args, context) => {
        return root.subjects.filter((sub) => sub.id === args.id);
    },
    patch: async (root, {input}, context) => {
        return await root.update(Object.assign({}, input));
    },
    delete: async (root, _, context) => {
        try {
            await root.destroy();
            return 'Successfully deleted!';
        } catch (e) {
            return 'Removal error!';
        }
    },
};
