import logger from '../../../../utils/logger';
import {Student, Professor} from '../../../sequelize';
import jwt from 'jsonwebtoken';
import config from 'config';
import {AuthorizationError} from '../../errors';
import checkAuthAndResolve from '../../utils/checkAuthAndResolve';
import controllers from '../../controllers/index';

const {getProfessorByIdOrEmail} = controllers;

export default {
    studentSignUp: async (root, {input}, context) => {
        try {
            await Student.create({
                firstName: input.firstName,
                lastName: input.lastName,
                email: input.email,
                passwordHash: input.password,
                avatarURL: input.avatarURL,
            });
            logger.info('User: ' + input.email + ' successfully added!');
            const payload = {
                user: {
                    id: input.id,
                    email: input.email,
                },
                scopes: [
                    'read:student',
                    'write:student',
                ],
            };

            return jwt.sign(
                payload,
                config.jwtSecret,
                {
                    expiresIn: 3600 * 24 * 7,
                },
            );
        } catch (e) {
            logger.error(e);
            throw new AuthorizationError({
                message: `The user does not created`,
            });
        }
    },
    professorSignUp: async (root, {input}, context) => {
        try {
            await Professor.create({
                firstName: input.firstName,
                lastName: input.lastName,
                email: input.email,
                passwordHash: input.password,
                avatarURL: input.avatarURL,
                about: input.about || '',
            });
            logger.info('User: ' + input.email + ' successfully added!');
            const payload = {
                user: {
                    id: input.id,
                    email: input.email,
                },
                scopes: [
                    'read:professor',
                    'write:professor',
                ],
            };

            return jwt.sign(
                payload,
                config.jwtSecret,
                {
                    expiresIn: 3600 * 24 * 7,
                },
            );
        } catch (e) {
            logger.error(e);
            throw new AuthorizationError({
                message: `The user does not created`,
            });
        }
    },
    getProfessor: (root, args, context) => {
        return checkAuthAndResolve(context, getProfessorByIdOrEmail);
    },
};
