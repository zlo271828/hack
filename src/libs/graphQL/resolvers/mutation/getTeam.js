import db from '../../../sequelize';

export default {
    addStudentToTeam: async (root, {input}, context) => {
        const teamMember = db.sequelize.models.teamMember;
        console.log(teamMember);
        return await teamMember.create({
            teamId: root[0].id,
            studentId: input.studentId,
        });
    },
    getStudent: async (root, args, context) => {
        return await root[0].getStudents().filter((student) => student.id === args.id);
    },
    patch: async (root, {input}, context) => {
        return await root[0].update(Object.assign({}, input));
    },
    delete: async (root, _, context) => {
        try {
            await root[0].destroy();
            return 'Successfully deleted!';
        } catch (e) {
            return 'Removal error!';
        }
    },
};
