export default {
    addGroup: async (root, {input}, context) => {
        return await root[0].createGroup(input);
    },
    getGroup: async (root, args, context) => {
        return await root[0].getGroups().filter((gr) => gr.tag === args.tag);
    },
    patch: async (root, {input}, context) => {
        return await root[0].update(Object.assign({}, input));
    },
    delete: async (root, _, context) => {
        try {
            await root[0].destroy();
            return 'Successfully deleted!';
        } catch (e) {
            return 'Removal error!';
        }
    },
};
