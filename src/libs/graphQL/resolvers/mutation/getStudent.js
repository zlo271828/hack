export default {
    addTask: async (root, {input}, context) => {
        return await root[0].createTask(input);
    },
    getTask: async (root, args, context) => {
        return await root[0].getTasks.filter((task) => task.id === args.id);
    },
};
