import {makeExecutableSchema} from 'graphql-tools';
import typeDefs from './types';
import resolvers from './resolvers/index';
import log from '../../utils/logger';
import {attachDirectives} from './directives';

const logger = {log: (e) => log.error(e)};

const Schema = makeExecutableSchema({
    typeDefs,
    resolvers,
    logger,
});

attachDirectives(Schema);

export default Schema;
