import db from '../../sequelize';

export default (root, args) => {
    return db.Student.findAll({
        include: [db.Task],
    });
};
