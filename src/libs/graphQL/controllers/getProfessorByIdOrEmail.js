import db from '../../sequelize';

export default function getSubjectByGroup(args) {
    return db.Professor.findOne({
        where: args.user,
        include: [db.Subject],
    });
};
