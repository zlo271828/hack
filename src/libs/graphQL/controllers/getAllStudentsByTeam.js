export default async (team) => {
    return await team.getStudents();
};
