import db from '../../sequelize';

export default (root, args) => {
    return db.Subject.findById(args.id, {
        include: [db.Group, db.Professor],
    });
};
