const Group = `
    type Group {
        tag: String!
        praepostor: Student
        subject: Subject
        teams: [Team]
    }
`;

export default Group;
