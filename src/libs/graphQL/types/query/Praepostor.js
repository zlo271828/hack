const Praepostor = `
    type Praepostor {
        id: String
        firstName: String
        lastName: String
        email: String!
        avatarURL: String
    }
`;

export default Praepostor;
