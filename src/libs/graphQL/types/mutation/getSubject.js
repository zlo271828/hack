const getSubject = `
    type getSubject {
        getGroup(tag: String!): getGroup
        addGroup(input: GroupInput!): Group
        delete: String
        patch(input: SubjectInput): Subject
    } 
`;

export default getSubject;
