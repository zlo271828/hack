const getStudent = `
    type getStudent {
        addTask(input: TaskInput!): Task
        getTask(id: String!): getTask
    } 
`;

export default getStudent;
