const getProfessor = `
    type getProfessor {
        getSubject(id: String!): getSubject
        addSubject(input: SubjectInput!): Subject
        delete: String
        patch(input: ProfessorInput): Professor
    } 
`;

export default getProfessor;
