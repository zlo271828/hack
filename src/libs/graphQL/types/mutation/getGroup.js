const getGroup = `
    type getGroup {
        getTeam(id: String!): getTeam
        addTeam(input: TeamInput!): Team
        delete: String
        patch(input: GroupInput): Group
    } 
`;

export default getGroup;
