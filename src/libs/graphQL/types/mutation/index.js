import RootMutation from './RootMutation';
import StudentInput from './StudentInput';
import ProfessorInput from './ProfessorInput';
import SubjectInput from './SubjectInput';
import getProfessor from './getProfessor';
import getSubject from './getSubject';
import getGroup from './getGroup';
import getTeam from './getTeam';
import TeamInput from './TeamInput';
import GroupInput from './GroupInput';
import TeamAddStudentInput from './TeamAddStudentInput';
import TaskInput from './TaskInput';
import getStudent from './getStudent';
import getTask from './getTask';

export default [
    GroupInput,
    getTask,
    getStudent,
    SubjectInput,
    TaskInput,
    TeamAddStudentInput,
    TeamInput,
    getTeam,
    getGroup,
    getSubject,
    SubjectInput,
    getProfessor,
    ProfessorInput,
    StudentInput,
    RootMutation,
];
