import {Professor, Student} from '../sequelize';
import passport from 'koa-passport';
import logger from '../../utils/logger';


passport.serializeUser(function(user, done) {
    const type = (user.group === undefined) ? Professor : Student;
    const key = {
        id: user.id,
        type,
    };
    done(null, key);
});

passport.deserializeUser(function(key, done) {
    const {id, type} = key;
    if (type === Student) {
        Student.findOne({where: {
                id: id,
            }}).then((res) => {
            done(null, res);
        }).catch((err) => {
            logger.error(err);
        });
    } else {
        Professor.findOne({where: {
                id: id,
            }}).then((res) => {
            done(null, res);
        }).catch((err) => {
            logger.error(err);
        });
    }
});
