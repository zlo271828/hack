import passport from 'koa-passport';
// import {Student, Professor} from '../sequelize';

require('./serialize');

require('./localStrategy');
require('./JWTStrategy');

module.exports = passport;
