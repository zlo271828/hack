import passport from 'koa-passport';
import config from 'config';
import {Strategy, ExtractJwt} from 'passport-jwt';
import logger from '../../utils/logger';
import {Professor, Student} from '../sequelize';

passport.use('jwtStudent', new Strategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.jwtSecret,
}, async function(jwtPayload, done) {
    let user;
    try {
        user = await Student.findOne({where: {
                id: jwtPayload.id,
            }});
    } catch (err) {
        logger.error(err);
        return done(err, false);
    }
    if (!user) {
        return done(null, false, {
            message:
                'User does not exist.',
        });
    }
    done(null, user);
}));

passport.use('jwtProfessor', new Strategy({
    jwtFromRequest: ExtractJwt.fromBodyField('authorization'),
    secretOrKey: config.jwtSecret,
}, async function(jwtPayload, done) {
    logger.info(jwtPayload);
    let user;
    try {
        user = await Professor.findOne({where: {
                id: jwtPayload.id,
            }});
    } catch (err) {
        logger.error(err);
        return done(err, false);
    }
    if (!user) {
        return done(null, false, {
            message:
                'User does not exist.',
        });
    }
    done(null, user);
}));
