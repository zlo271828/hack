import Sequelize from 'sequelize';
import config from 'config';
import log from '../../utils/logger';
import fs from 'fs';
import path from 'path';
// import _ from 'lodash';
// import faker from 'faker';

const dbconfig = config.database;
log.info(`postgres://${dbconfig.login}:${dbconfig.password}@${process.env.POSTGRES_ADDRESS || dbconfig.address}:${dbconfig.port}/${dbconfig.name}`);
const sequelize = new Sequelize(`postgres://${dbconfig.login}:${dbconfig.password}@${process.env.POSTGRES_ADDRESS || dbconfig.address}:${dbconfig.port}/${dbconfig.name}`,
    {
    // logging: log.info.bind(log),
});

let db = {};
db.sequelize = sequelize;
db.Sequelize = Sequelize;

async function dbInit(db, force = false, fixtures = false) {
    try {
        await db.sequelize.authenticate();
        log.info('Connection has been established successfully.');
        const models = fs.readdirSync(path.join(__dirname, '../../models'));
        models.forEach(function(model) {
            db[model] = sequelize.import(path.join(__dirname, '../../models/') + model);
        });
    } catch (e) {
        log.error('Unable to connect to the database:', e);
    }
    Object.keys(db).forEach((modelName) =>{
        if ('associate' in db[modelName]) {
            db[modelName].associate(db);
        }
    });
    try {
        await db.sequelize.sync({force: force});
        log.info('Successful synchronization');
    } catch (e) {
        log.error('Synchronization failed!\n' + e);
    }
    if (fixtures) {
        await fake();
    }
}

dbInit(db, !!process.env.POSTGRESS_FORCE || dbconfig.force, dbconfig.fixtures);

module.exports = db;

async function fake() {
    // await db.Subject.create({
    //     subjectName: faker.commerce.product(),
    //     about: faker.commerce.productMaterial(),
    //     filters: ['New', 'LR_3'],
    // });
    // await db.Subject.create({
    //     subjectName: faker.commerce.product(),
    //     about: faker.commerce.productMaterial(),
    //     filters: ['Old', 'LR_1'],
    // });
    // const subjects = await db.Subject.findAll();
    // subjects.forEach((subject) => {
    //     _.times(Math.floor(Math.random() * (5)) + 2, async () => {
    //         return await subject.createProfessor({
    //             firstName: faker.name.firstName(),
    //             lastName: faker.name.lastName(),
    //             email: faker.internet.email(),
    //             about: faker.hacker.phrase(),
    //             passwordHash: faker.internet.password(),
    //             avatarURL: faker.image.avatar(),
    //         });
    //     });
    // });
    // const groups = await db.Group.findAll();
    // groups.forEach((group) => {
    //     return _.times(Math.floor(Math.random() * (5)) + 2, async () => {
    //         return await group.createTeam({
    //             teamName: faker.name.jobType(),
    //         });
    //     });
    // });
    // const teams = await db.Team.findAll();
    // teams.forEach((team) => {
    //     return _.times(Math.floor(Math.random() * (5)) + 2, async () => {
    //         return await team.createStudent({
    //             firstName: faker.name.firstName(),
    //             lastName: faker.name.lastName(),
    //             email: faker.internet.email(),
    //             passwordHash: faker.internet.password(),
    //             avatarURL: faker.image.avatar(),
    //         });
    //     });
    // });
    // const students = await db.Student.findAll();
    // students.forEach((student) => {
    //     return _.times(Math.floor(Math.random() * (5)) + 2, async () => {
    //         return await student.createTask({
    //             task: {
    //                 title: faker.name.jobTitle(),
    //                 description: faker.name.jobType(),
    //             },
    //             mark: 'F',
    //         });
    //     });
    // });
}
