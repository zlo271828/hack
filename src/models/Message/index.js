module.exports = (sequelize, DataTypes) => {
    const Message = sequelize.define('message', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        text: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        indexes: [
            {
                fields: ['id'],
            },
        ],
    });

    Message.associate = (db) => {
        Message.belongsTo(db.Professor);
        Message.belongsTo(db.Student);
    };
    return Message;
};
