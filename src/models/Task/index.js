module.exports = (sequelize, DataTypes) => {
    const Task = sequelize.define('task', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        task: {
            type: DataTypes.JSONB,
            required: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false,
        },
        mark: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    }, {
        indexes: [
            {
                fields: ['id'],
            },
        ],
    });

    Task.associate = (db) => {
        Task.belongsTo(db.Student);
    };
    return Task;
};
