module.exports = (sequelize, DataTypes) => {
    const Group = sequelize.define('group', {
        tag: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
    }, {
        indexes: [
            {
                fields: ['tag'],
            },
        ],
        timestamps: false,
    });
    Group.associate = (db) => {
        Group.belongsToMany(db.Team, {
            through: 'groupMember',
            foreignKey: 'groupTag',
            constraints: false,
        });
        Group.belongsToMany(db.Subject, {
            through: 'subjectMember',
            constraints: false,
        });
    };
    return Group;
};


