module.exports = (sequelize, DataTypes) => {
    const Subject = sequelize.define('subject', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        subjectName: {
            type: DataTypes.STRING,
            required: true,
            validate: {
                len: [2, 30],
            },
        },
        filters: {
            type: DataTypes.ARRAY(DataTypes.STRING), // eslint-disable-line
        },
        about: {
            type: DataTypes.TEXT,
        },
    }, {
        indexes: [
            {
                fields: ['id'],
            },
        ],
        timestamps: false,
    });
    Subject.associate = (db) => {
        Subject.belongsToMany(db.Group, {
            through: 'subjectMember',
            constraints: false,
        });
        Subject.belongsTo(db.Professor);
    };
    return Subject;
};
