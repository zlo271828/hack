FROM keymetrics/pm2:latest-jessie

RUN mkdir /hack-app
COPY package.json /hack-app

WORKDIR /hack-app

RUN npm install

COPY . /hack-app

EXPOSE 3000

RUN ls /hack-app/config

RUN export NODE_CONFIG_DIR=/hack-app/config

RUN echo $NODE_CONFIG_DIR

CMD [ "pm2-runtime", "start", "pm2.json" ]

# CMD [ "npm", "run", "start_cluster" ]

# CMD [ "node", "dist/server.js" ]
